<?php

namespace Easytek\GuestbookBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MessageAdminType extends MessageType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	parent::buildForm($builder, $options);
    	
        $builder
        	->remove('captcha')
        ;
    }

    public function getName()
    {
        return 'easytek_guestbookbundle_messageadmintype';
    }
}
