<?php

namespace Easytek\GuestbookBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', 'text', array('label' => 'easytek.guestbook.votre_nom'))
            ->add('email', 'email', array('label' => 'easytek.guestbook.votre_email', 'required' => false))
            ->add('text', 'textarea', array('label' => 'easytek.guestbook.votre_message'))
            ->add('captcha', 'captcha', array(
            	'label' => 'easytek.guestbook.code_de_vérification',
            	'attr' => array(
            			'class' => 'span2',
            	)
            ))
        ;
    }

    public function getName()
    {
        return 'easytek_guestbookbundle_messagetype';
    }
}
