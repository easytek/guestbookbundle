<?php

namespace Easytek\GuestbookBundle\Controller;

use Easytek\GuestbookBundle\Form\MessageAdminType;

use Symfony\Component\HttpFoundation\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Easytek\GuestbookBundle\Entity\Message;
use Easytek\GuestbookBundle\Form\MessageType;
use Symfony\Component\HttpFoundation\Request;

class GuestbookController extends Controller
{
	/**
	 * @Route("/guestbook", name="ecms_guestbook_index")
	 * @Template()
	 */
	public function indexAction()
    {
    	return $this->messagesAction();
    }

    /**
     * @Template()
     */
    public function messagesAction(Request $request)
    {
    	$message = new Message();
    	$form = $this->createForm(new MessageType(), $message);
    	$em = $this->getDoctrine()->getManager();
    	 
    	if ($request->getMethod() == 'POST') {
    		$form->handleRequest($request);
    		 
    		if ($form->isValid()) {
    			$em->persist($message);
    			$em->flush();
    			
    			$destinataire = $this->get("ecms.configuration")->get('general', 'email_dest');
    			$expediteur = $this->get("ecms.configuration")->get('general', 'email_exp');
    			
    			$nomSite =  $this->get("ecms.configuration")->get('general', 'title');
    			$sujet = sprintf("[%s] %s a laissé un message sur votre livre d'or", $nomSite, $message->getAuthor());
    			
    			$message = \Swift_Message::newInstance()
	    			->setSubject($sujet)
	    			->setFrom($expediteur)
	    			->setTo($destinataire)
	    			->setBody($message->getText())
    			;
    			
    			$this->get('mailer')->send($message);
    		}
    	}
    	
    	$messages = $em->getRepository('EasytekGuestbookBundle:Message')->findAllOrderedByCreated();

    	return array(
			'messages' => $messages,
			'form'   => $form->createView()
    	);
    }
    
    /**
     * Lists all Message entities.
     *
     * @Route("/guestbook/message", name="ecms_guestbook")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function messageListAction()
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entities = $em->getRepository('EasytekGuestbookBundle:Message')->findAll();
    
    	return array('entities' => $entities);
    }
    
    /**
     * Finds and displays a Message entity.
     *
     * @Route("/guestbook/{id}/show", name="ecms_guestbook_show")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function showAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('EasytekGuestbookBundle:Message')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Message entity.');
    	}
    
    	$deleteForm = $this->createDeleteForm($id);
    
    	return array(
    		'entity'      => $entity,
    		'delete_form' => $deleteForm->createView(),
    	);
    }
    
    /**
     * Displays a form to create a new Message entity.
     *
     * @Route("/new", name="ecms_guestbook_new")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction()
    {
    	$entity = new Message();
    	$form   = $this->createForm(new MessageAdminType(), $entity);
    
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView()
    	);
    }
    
    /**
     * Creates a new Message entity.
     *
     * @Route("/guestbook/create", name="ecms_guestbook_create")
     * @Method("post")
     * @Template("EasytekGuestbookBundle:Message:new.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction()
    {
    	$entity  = new Message();
    	$request = $this->getRequest();
    	$form    = $this->createForm(new MessageAdminType(), $entity);
    	$form->handleRequest($request);
    
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('ecms_guestbook_show', array('id' => $entity->getId())));
    	}
    
    	return array(
    		'entity' => $entity,
    		'form'   => $form->createView()
    	);
    }
    
    /**
     * Displays a form to edit an existing Message entity.
     *
     * @Route("/guestbook/{id}/edit", name="ecms_guestbook_edit")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('EasytekGuestbookBundle:Message')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Message entity.');
    	}
    
    	$editForm = $this->createForm(new MessageAdminType(), $entity);
    	$deleteForm = $this->createDeleteForm($id);
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'delete_form' => $deleteForm->createView(),
    	);
    }
    
    /**
     * Edits an existing Message entity.
     *
     * @Route("/guestbook/{id}/update", name="ecms_guestbook_update")
     * @Method("post")
     * @Template("EasytekGuestbookBundle:Message:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction($id)
    {
    	$em = $this->getDoctrine()->getManager();
    
    	$entity = $em->getRepository('EasytekGuestbookBundle:Message')->find($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('Unable to find Message entity.');
    	}
    
    	$editForm   = $this->createForm(new MessageAdminType(), $entity);
    	$deleteForm = $this->createDeleteForm($id);
    
    	$request = $this->getRequest();
    
    	$editForm->bindRequest($request);
    
    	if ($editForm->isValid()) {
    		$em->persist($entity);
    		$em->flush();
    
    		return $this->redirect($this->generateUrl('ecms_guestbook_edit', array('id' => $id)));
    	}
    
    	return array(
    		'entity'      => $entity,
    		'edit_form'   => $editForm->createView(),
    		'delete_form' => $deleteForm->createView(),
    	);
    }
    
    /**
     * Deletes a Message entity.
     *
     * @Route("/guestbook/{id}/delete", name="ecms_guestbook_delete")
     * @Method("post")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction($id)
    {
    	$form = $this->createDeleteForm($id);
    	$request = $this->getRequest();
    
    	$form->handleRequest($request);
    
    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		$entity = $em->getRepository('EasytekGuestbookBundle:Message')->find($id);
    
    		if (!$entity) {
    			throw $this->createNotFoundException('Unable to find Message entity.');
    		}
    
    		$em->remove($entity);
    		$em->flush();
    	}
    
    	return $this->redirect($this->generateUrl('ecms_guestbook'));
    }
    
    private function createDeleteForm($id)
    {
    	return $this->createFormBuilder(array('id' => $id))
	    	->add('id', 'hidden')
	    	->getForm()
    	;
    }
}
