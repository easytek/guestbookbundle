<?php

namespace Easytek\GuestbookBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easytek\EcmsBundle\Entity\Page;

class LoadPageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$pages = array();
    	
        $page = new Page();
        $page
        	->setTitre('guestbook')
        	->setHtml("<h2>Livre d'or</h2>")
        	->setEmbeded(true)
        	->setLinkable(false)
        ;
        
		$manager->persist($page);
        $manager->flush();
    }
    
    public function getOrder()
    {
    	return 2;
    }
}