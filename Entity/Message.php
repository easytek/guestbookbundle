<?php

namespace Easytek\GuestbookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Easytek\GuestbookBundle\Entity\Message
 *
 * @ORM\Table(name="ecms_guestbook_message")
 * @ORM\Entity(repositoryClass="Easytek\GuestbookBundle\Entity\MessageRepository")
 */
class Message
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text $text
     *
     * @ORM\Column(name="text", type="text")
     * 
     * @Assert\NotBlank()
     */
    private $text;

    /**
     * @var string $author
     *
     * @ORM\Column(name="author", type="string", length=100)
     * 
     * @Assert\NotBlank()
     * @Assert\Length(max = "100")
     */
    private $author;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * 
     * @Assert\Email()
     */
    private $email;
    
    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param text $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Get text
     *
     * @return text 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set author
     *
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
    	$this->created = $created;
    }
    
    /**
     * Get created
     *
     * @return datetime
     */
    public function getCreated()
    {
    	return $this->created;
    }
}