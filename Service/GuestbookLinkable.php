<?php

namespace Easytek\GuestbookBundle\Service;

use Easytek\EcmsBundle\Service\Linkable;

class GuestbookLinkable extends Linkable
{
	public function __construct($router, $configuration)
	{
		$this->router = $router;
		$this->configuration = $configuration;
		
		$this->publicName = "Livre d'or";
		
		$this->addRoute($this->getPublicName(), 'ecms_guestbook_index');
		
		$this->addAdminRoute($this->getPublicName(), 'ecms_guestbook');
	}
}
